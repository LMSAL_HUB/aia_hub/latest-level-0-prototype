#!/usr/bin/env perl
use strict;
use warnings;
use Glib qw/TRUE FALSE/;
use Gtk2 qw/ -init -threads-init/;
use Getopt::Long;

my ($i, $idir, $fn, $w, $mn, @images, $box1, $box2, $window, $vbox, $timer);
my ($speed_label, $first_entry, $last_entry, $reverse_button);
my ($stop_button, $gray_button);
my $bwdir = '/surge40/jps/past_hour_lev0';
my $cdir = '/surge40/jps/past_hour_lev0_color';
my @ws = (94, 131, 171, 193, 211, 304, 335, 1600);
my $timeout = 450; my $dir = 1; my $first_min = 0; my $last_min = 59;

sub stop {
  if ($stop_button->get_label() =~ /Stop/) {
    $stop_button->set_label('Start');
    return Glib::Source->remove($timer);
  } else {
    $timer = Glib::Timeout->add ($timeout, sub{ &update_images(); });
    $stop_button->set_label('Stop');
    return TRUE;
  }
}

sub update_images {
  $mn = $mn + $dir;
  $first_min = $first_entry->get_text();
  $last_min = $last_entry->get_text();
  if ($mn < $first_min) { $mn = $last_min; }
  if ($mn > $last_min) { $mn = $first_min; }
  for (my $i=0; $i<8; $i++) {
    my $fn = sprintf "$idir/%.2d_%.2d.png", $ws[$i], $mn;
    Gtk2::Image::set_from_file($images[$i], $fn);
  }
  return 1;
}

sub faster {
  $timeout -= 10;
  $speed_label->set_markup("<span>Rate $timeout ms</span>");
  Glib::Source->remove($timer);
  $timer = Glib::Timeout->add ($timeout, sub{ &update_images(); });
  return TRUE;
}

sub slower {
  $timeout += 10;
  $speed_label->set_markup("<span>Rate $timeout ms</span>");
  Glib::Source->remove($timer);
  $timer = Glib::Timeout->add ($timeout, sub{ &update_images(); });
  return TRUE;
}

sub reverse {
  $dir *= -1;
  my $label = $reverse_button->get_label();
  if ($label =~ /Reverse/) { $reverse_button->set_label('Forward'); }
  else { $reverse_button->set_label('Reverse'); }
  return TRUE;
}

sub color {
  if ($gray_button->get_label() =~ /Color/) {
    $gray_button->set_label('Gray');
    $idir = $cdir;
  } else {
    $gray_button->set_label('Color');
    $idir = $bwdir;
  }
  return TRUE;
}

GetOptions(
           "bwdir=s" => \$bwdir,
           "cdir=s"  => \$cdir
          );

$idir = $bwdir;
$window = Gtk2::Window->new('toplevel');
$vbox = Gtk2::VBox->new(FALSE, 0);
$box1 =  Gtk2::HBox->new(TRUE, 0);
$box2 =  Gtk2::HBox->new(TRUE, 0);
my $box3 =  Gtk2::HBox->new(FALSE, 0);
$window->signal_connect(delete_event => sub { Gtk2->main_quit });
my $first_label = Gtk2::Label->new('First minute:');
$first_label->set_alignment(1, 0.5);
$first_entry = Gtk2::Entry->new();
$first_entry->set_width_chars(2);
$first_entry->set_alignment(0);
$first_entry->set_text($first_min);
my $last_label = Gtk2::Label->new('Last minute:');
$last_label->set_alignment(1, 0.5);
$last_entry = Gtk2::Entry->new();
$last_entry->set_width_chars(2);
$last_entry->set_alignment(1);
$last_entry->set_text($last_min);
$gray_button = Gtk2::Button->new ('Color');
$gray_button->signal_connect (clicked => sub { color(); });
$stop_button = Gtk2::Button->new ('Stop');
$stop_button->signal_connect (clicked => sub { stop(); });
my $update_button = Gtk2::Button->new ('Update');
$update_button->signal_connect (clicked => sub { update_images(); });
$speed_label = Gtk2::Label->new();
$speed_label->set_markup("<span>Rate $timeout ms</span>");
my $faster_button = Gtk2::Button->new ('Faster');
$faster_button->signal_connect (clicked => sub { faster(); });
my $slower_button = Gtk2::Button->new ('Slower');
$slower_button->signal_connect (clicked => sub { slower(); });
$reverse_button = Gtk2::Button->new ('Reverse');
$reverse_button->signal_connect (clicked => sub { &reverse(); });
my $quit_button = Gtk2::Button->new ('Quit');
$quit_button->signal_connect (clicked => sub { Gtk2->main_quit });
$vbox->pack_start($box1, TRUE, FALSE, 0);
$vbox->pack_start($box2, TRUE, FALSE, 0);
$vbox->pack_start($box3, FALSE, FALSE, 0);
$window->add($vbox);
$mn = 0;
for ($i=0; $i<8; $i++) {
  $fn = sprintf "$idir/%.2d_%.2d.png", $ws[$i], $mn;
  $images[$i] = Gtk2::Image->new_from_file($fn);
  if ($i<4) { $box1->pack_start($images[$i], TRUE, TRUE, 0); }
  else { $box2->pack_start($images[$i], TRUE, TRUE, 0); }
  $images[$i]->show;
}
$box3->pack_start($first_label, FALSE, FALSE, 10);
$box3->pack_start($first_entry, FALSE, FALSE, 10);
$box3->pack_start($last_label, FALSE, FALSE, 10);
$box3->pack_start($last_entry, FALSE, FALSE, 10);
$box3->pack_start($gray_button, FALSE, FALSE, 10);
$box3->pack_start($stop_button, FALSE, FALSE, 10);
$box3->pack_start($update_button, FALSE, FALSE, 10);
$box3->pack_start($reverse_button, FALSE, FALSE, 10);
$box3->pack_start($speed_label, FALSE, FALSE, 10);
$box3->pack_start($faster_button, FALSE, FALSE, 10);
$box3->pack_start($slower_button, FALSE, FALSE, 10);
$box3->pack_start($quit_button, FALSE, FALSE, 10);
$window->show_all;
$timer = Glib::Timeout->add ($timeout, sub{ &update_images(); });
Gtk2->main();
