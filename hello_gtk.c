#include <gtk/gtk.h>

int main( int   argc, char *argv[] )
{
    GtkWidget *window;
    GtkWidget *image; // = NULL;
    gtk_init (&argc, &argv);
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    image = gtk_image_new_from_file ("img.png");
    gtk_container_add (GTK_CONTAINER (window), image);
    gtk_widget_show  (image);
    gtk_widget_show  (window);
    gtk_main ();
    return 0;
}
