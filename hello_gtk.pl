#!/usr/bin/env perl
use strict;
use warnings;
use Glib qw/TRUE FALSE/;
use Gtk2 '-init';

sub hello {
  print "Hello World\n";
}

sub delete_event {
  print "delete event occurred\n";
  return TRUE;
}

sub destroy {
  Gtk2->main_quit();
}

my $window = Gtk2::Window->new('toplevel');
my $vbox = Gtk2::VBox->new(TRUE, 0);
$window->add($vbox);
my $image = Gtk2::Image->new_from_file('img.png');
#$window->add($image);
$window->signal_connect(delete_event => sub { Gtk2->main_quit });
	my $button = Gtk2::Button->new ('Action');
 	$button->signal_connect (clicked => sub { 
 	
   		print("Hello Gtk2-Perl\n");
 		
   	});
my $quit_button = Gtk2::Button->new ('Quit');
$quit_button->signal_connect (clicked => sub { Gtk2->main_quit });
$vbox->add ($button);
$vbox->add ($quit_button);
#$image->show;
#$vbox->show;
#$window->show;
$window->show_all;
Gtk2->main();
