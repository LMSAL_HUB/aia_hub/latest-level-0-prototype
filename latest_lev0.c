#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>

int i, mn, w;
int dir = 1, first_min = 0, last_min = 59;
const char *bwdir, *cdir, *idir; char inam[256], label[32];
guint timer, timeout=450;
GtkWidget *window, *vbox, *box1, *box2, *box3, *images[8];
GtkWidget *speed_label, *first_entry, *last_entry, *reverse_button;
GtkWidget *bwdir_label, *bwdir_entry, *cdir_label, *cdir_entry;
GtkWidget *stop_button, *gray_button, *quit_button, *faster_button;
GtkWidget *slower_button, *update_button, *first_label, *last_label;
int ws[] = { 94, 131, 171, 193, 211, 304, 335, 1600 };

int update_images() {
  mn = mn + dir;
  sscanf(gtk_entry_get_text((GtkEntry *) first_entry), "%d", &first_min);
  sscanf(gtk_entry_get_text((GtkEntry *) last_entry), "%d", &last_min);
  if (mn<first_min) mn = last_min;
  if (mn>last_min) mn = first_min;
  for (i=0; i<8; i++) {
    sprintf(inam, "%s/%d_%.2d.png", idir, ws[i], mn);
    gtk_image_set_from_file ((GtkImage *) images[i], inam);
  }
  return 1;
}

int color() {
  if (strcmp(gtk_button_get_label((GtkButton *) gray_button), "Gray")) {
    gtk_button_set_label((GtkButton *) gray_button, "Gray");
    cdir = gtk_entry_get_text((GtkEntry *) cdir_entry);
    idir = cdir;
  } else {
    gtk_button_set_label((GtkButton *) gray_button, "Color");
    bwdir = gtk_entry_get_text((GtkEntry *) bwdir_entry);
    idir = bwdir;
  }
  return 1;
}

int stop() {
  if (strcmp(gtk_button_get_label((GtkButton *) stop_button), "Start")) {
    gtk_button_set_label((GtkButton *) stop_button, "Start");
    return g_source_remove(timer);
  } else {
    gtk_button_set_label((GtkButton *) stop_button, "Stop");
    timer = g_timeout_add(timeout, &update_images, NULL);
  }
  return 1;
}

int reverse() {
  dir *= -1;
  if (strcmp(gtk_button_get_label((GtkButton *) reverse_button), "Forward")) {
    gtk_button_set_label((GtkButton *) reverse_button, "Forward");
  } else gtk_button_set_label((GtkButton *) reverse_button, "Reverse");
  return 1;
}

int faster() {
  timeout -= 10;
  sprintf(label, "Rate: %d ms", timeout);
  gtk_label_set_text((GtkLabel *) speed_label, label);
  g_source_remove(timer);
  timer = g_timeout_add(timeout, &update_images, NULL);
  return 1;
}

int slower() {
  timeout += 10;
  sprintf(label, "Rate: %d ms", timeout);
  gtk_label_set_text((GtkLabel *) speed_label, label);
  g_source_remove(timer);
  timer = g_timeout_add(timeout, &update_images, NULL);
  return 1;
}

int main( int   argc, char *argv[] )
{
    char *s;
    bwdir = "/surge40/jps/past_hour_lev0";
    cdir = "/surge40/jps/past_hour_lev0_color";
    gtk_init (&argc, &argv);
    while ((s = *++argv)) {
    if (*s == '-') {
        switch (*++s) {
            case 'b': bwdir = *++argv; break;
            case 'c': cdir = *++argv; break;
            default: fprintf(stderr, "unknown argument: '%s'\n", s); exit(1);
        }
      }
    }
    idir = bwdir;
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    g_signal_connect(window, "delete_event", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
#ifdef GTK3
    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    box1 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    box3 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
#else
    vbox = gtk_vbox_new(FALSE, 0);
    box1 = gtk_hbox_new(TRUE, 0);
    box2 = gtk_hbox_new(TRUE, 0);
    box3 = gtk_hbox_new(FALSE, 0);
#endif
    gtk_container_add (GTK_CONTAINER (window), vbox);
    gtk_box_pack_start(GTK_BOX(vbox), box1, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), box2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), box3, FALSE, FALSE, 0);
    first_label = gtk_label_new("First minute:");
    gtk_box_pack_start(GTK_BOX(box3), first_label, FALSE, FALSE, 10);
    first_entry = gtk_entry_new();
    sprintf(label, "%d", first_min);
    gtk_entry_set_text((GtkEntry *) first_entry, label);
    gtk_entry_set_width_chars((GtkEntry *) first_entry, 2);
    gtk_box_pack_start(GTK_BOX(box3), first_entry, FALSE, FALSE, 10);
    last_label = gtk_label_new("Last minute:");
    gtk_box_pack_start(GTK_BOX(box3), last_label, FALSE, FALSE, 10);
    last_entry = gtk_entry_new();
    sprintf(label, "%d", last_min);
    gtk_entry_set_text((GtkEntry *) last_entry, label);
    gtk_entry_set_width_chars((GtkEntry *) last_entry, 2);
    gtk_box_pack_start(GTK_BOX(box3), last_entry, FALSE, FALSE, 10);
    bwdir_label = gtk_label_new("Gray Dir:");
    gtk_box_pack_start(GTK_BOX(box3), bwdir_label, FALSE, FALSE, 10);
    bwdir_entry = gtk_entry_new();
    gtk_entry_set_width_chars((GtkEntry *) bwdir_entry, 32);
    gtk_entry_set_text((GtkEntry *) bwdir_entry, bwdir);
    gtk_box_pack_start(GTK_BOX(box3), bwdir_entry, FALSE, FALSE, 10);
    cdir_label = gtk_label_new("Color Dir:");
    gtk_box_pack_start(GTK_BOX(box3), cdir_label, FALSE, FALSE, 10);
    cdir_entry = gtk_entry_new();
    gtk_entry_set_width_chars((GtkEntry *) cdir_entry, 32);
    gtk_entry_set_text((GtkEntry *) cdir_entry, cdir);
    gtk_box_pack_start(GTK_BOX(box3), cdir_entry, FALSE, FALSE, 10);
    gray_button = gtk_button_new_with_label("Color");
    g_signal_connect(gray_button, "clicked", G_CALLBACK(color), NULL);
    gtk_box_pack_start(GTK_BOX(box3), gray_button, FALSE, FALSE, 10);
    stop_button = gtk_button_new_with_label("Stop");
    g_signal_connect(stop_button, "clicked", G_CALLBACK(stop), NULL);
    gtk_box_pack_start(GTK_BOX(box3), stop_button, FALSE, FALSE, 10);
    reverse_button = gtk_button_new_with_label("Reverse");
    g_signal_connect(reverse_button, "clicked", G_CALLBACK(reverse), NULL);
    gtk_box_pack_start(GTK_BOX(box3), reverse_button, FALSE, FALSE, 10);
    update_button = gtk_button_new_with_label("Update");
    g_signal_connect(update_button, "clicked", G_CALLBACK(update_images), NULL);
    gtk_box_pack_start(GTK_BOX(box3), update_button, FALSE, FALSE, 10);
    sprintf(label, "Rate: %d ms", timeout);
    speed_label = gtk_label_new(label);
    gtk_box_pack_start(GTK_BOX(box3), speed_label, FALSE, FALSE, 10);
    faster_button = gtk_button_new_with_label("Faster");
    g_signal_connect(faster_button, "clicked", G_CALLBACK(faster), NULL);
    gtk_box_pack_start(GTK_BOX(box3), faster_button, FALSE, FALSE, 10);
    slower_button = gtk_button_new_with_label("Slower");
    g_signal_connect(slower_button, "clicked", G_CALLBACK(slower), NULL);
    gtk_box_pack_start(GTK_BOX(box3), slower_button, FALSE, FALSE, 10);
    quit_button = gtk_button_new_with_label("Quit");
    g_signal_connect(quit_button, "clicked", G_CALLBACK(gtk_main_quit), NULL);
    gtk_box_pack_start(GTK_BOX(box3), quit_button, FALSE, FALSE, 10);
    mn = 0;
    for (i=0; i<8; i++) {
      sprintf(inam, "%s/%d_%.2d.png", idir, ws[i], mn);
      images[i] = gtk_image_new_from_file (inam);
      if (i<4) gtk_box_pack_start(GTK_BOX(box1), images[i], TRUE, TRUE, 0);
      else gtk_box_pack_start(GTK_BOX(box2), images[i], TRUE, TRUE, 0);
      gtk_widget_show(images[i]);
    }
    gtk_widget_show_all(window);
    timer = g_timeout_add(timeout, &update_images, NULL);
    gtk_main ();
    return 0;
}
